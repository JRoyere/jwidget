<?php



/**
 * Displays all testimonials
 */

class JWidgetall extends WP_Widget 

{

    public function __construct() {

        parent::WP_Widget(

            'JWidgetall', 
            
            //title of the widget in the WP dashboard
            __('All Testimonials'), 

            array('description'=>'Display all testimonials in one widget', 'class'=>'JWidgetallwidget')

        );

    }

    

    /**
     * 
     * @param type $instance
     */

    public function form($instance)

    {
        // these are the default widget values
        $default = array( 

            'title' => __(''),

            'code'=> __('')

            );

        $instance = wp_parse_args( (array)$instance, $default );

        //this is the html for the fields in the wp dashboard
        echo "\r\n";

        echo "<p>";

        echo "<label for='".$this->get_field_id('title')."'>" . __('Title') . ":</label> " ;

        echo "<input type='text' class='widefat' id='".$this->get_field_id('title')."' name='".$this->get_field_name('title')."' value='" . esc_attr($instance['title'] ) . "' />" ;

        echo "</p>";

        echo "<p>";

        echo "<label for='".$this->get_field_id('code')."'>" . __('What do you want wrapped?') . ":</label> " ;

        echo "<input type='text' class='widefat' id='".$this->get_field_id('code')."' name='".$this->get_field_name('code')."' value='" . esc_attr( $instance['code'] ) . "' placeholder='This shows up as a watermark in the field.' />" ;

        echo "</p>";

    }

        

    /**
     * 
     * @param type $new_instance
     * @param type $old_instance
     * @return type
     */

    public function update($new_instance, $old_instance) 

    {

        $instance = $old_instance;

        $instance['title'] = strip_tags($new_instance['title']);

        $instance['code'] = $new_instance['code'];

        return $instance;

    }

        

    /**
     * Renders the actual widget
     * 
     * @global post $post
     * @param array $args 
     * @param type $instance
     */

    
    
    public function widget($args, $instance) 

    {

        extract($args, EXTR_SKIP);
        
    
        echo $before_widget;

     
          global $wpdb;
          
          $table_name = $wpdb->prefix . 'cte';
      

          $thearr = $wpdb->get_results("SELECT * FROM {$table_name}", ARRAY_A);
           
           $arrlength = count($thearr);

         //Gerates random number for msg arrayy
         $rdm = rand(0, $arrlength );

          ?> <style type="text/css">


.box {
width: 130px;
    margin: 0px auto;
    padding: 12px;
    text-align: center;
    border: 1px solid grey;
    color: grey;
}


.box:hover {
  transition: .8s ease;
  background-color: grey;
  border: 1px solid white;
}

.box:hover a {
    transition: .8s ease-in;
    color: #fff;
}

.overlay {
position: absolute;
top: 0;
left: 0;
right: 0;
bottom: 0;
background: rgba(0, 0, 0, 0.7) none repeat scroll 0% 0%;
transition: opacity 500ms ease 0s;
z-index: 999999;
 visibility: hidden;
  opacity: 0;
}
.overlay:target {
  visibility: visible;
  opacity: 1;
}

.review-txt {
  text-align: center; font-size: 16px; color: grey; min-height: 125px; padding: 0px 5px; white-space: pre-wrap; 
}

.profile {

text-align: center; padding: 0; max-width: 170px; margin: auto; min-height: 170px; 
}

.popup {
  padding: 20px;
  background: #FFF none repeat scroll 0% 0%;
  border-radius: 5px;
  width: 60%;
  position: relative;
  transition: all 1.5s ease-in-out 0s;
  top: 25%;
  margin: 0 auto;
}

.popup h2 {
  margin-top: 0;
  color: #333;
  font-family: Tahoma, Arial, sans-serif;
}
.popup .close {
  position: absolute;
  top: 20px;
  right: 30px;
  transition: all 200ms;
  font-size: 30px;
  font-weight: bold;
  text-decoration: none;
  color: #333;
}
.popup .close:hover {
}
.popup .content {
  max-height: 30%;
  overflow: auto;
}

 .Jwidalign{
  width: 32.8%; float: left;
  padding-bottom: 35px;
  }

@media screen and (max-width: 800px){
  .box{
    width: 70%;
  }
  .popup{
    width: 70%;
  top: 200px;

  }
  
  .Jwidalign{
  width: 100%; float: left;
  
  }

  .box {
  margin: 0px auto;
  padding-bottom: 20px;
}
}


.thestar {
  
  content: "★";
  color: #FFED85;
  text-shadow: 0 0 1px #333;
  font-size: 1.8em;
}
.thatstar{
  content: "☆";
  color: #cfcfcf;
  text-shadow: none;
   font-size: 1.8em;
}


</style>    <?php
//wrap
       echo '<div class="wrap" style="text-align: center;">';

      for ($i = 0; $i < $arrlength; $i++) {


        
        //Profile image
        echo '<div class="Jwidalign" "> <div class="profile" > <img src="';
        echo $thearr[$i]['url'];
        echo '"/></div>';
        
        //Name of reviewer
        echo '<div style="text-align: center;"><h2>';
        echo $thearr[$i]['name'];
        echo '</h2></div>';
            
        //Start review generates here   
        echo '<div style = " text-align: center; padding-bottom: 20px; margin: auto; "> ';
         if($thearr[$i]['rating'] === "&#9733 &#9733 &#9733 &#9733 &#9733" || $thearr[$i]['rating'] === "★ ★ ★ ★ ★"){ echo '<div class="thestar">  ★ ★ ★ ★ ★ </div>';}
        if($thearr[$i]['rating'] === "&#9733 &#9733 &#9733 &#9733 &#9734" || $thearr[$i]['rating'] === "★ ★ ★ ★ ☆"){ echo '<span class="thestar">  ★ ★ ★ ★</span> <span class="thatstar">★ </span>';}
        if($thearr[$i]['rating'] === "&#9733 &#9733 &#9733 &#9734 &#9734" || $thearr[$i]['rating'] === "★ ★ ★ ☆ ☆"){ echo '<span class="thestar">  ★ ★ ★ </span> <span class="thatstar">★ ★ </span>';}
        if($thearr[$i]['rating'] === "&#9733 &#9733 &#9734 &#9734 &#9734" || $thearr[$i]['rating'] === "★ ★ ☆ ☆ ☆"){ echo '<span class="thestar">  ★ ★ </span> <span class="thatstar">★ ★ ★ </span>';}
        if($thearr[$i]['rating'] === "&#9733 &#9734 &#9734 &#9734 &#9734" || $thearr[$i]['rating'] === "★ ☆ ☆ ☆ ☆"){ echo '<span class="thestar">  ★  </span> <div class="thatstar">★ ★ ★ ★ </span>';}
        echo '</div>';
        
        //$review1 = str_replace("\'","'",$thearr[$i]['review']);
        $review1 = str_replace("/n","<br>",$thearr[$i]['review']);



    
        //if statement limits review size
        if (strlen($review1) > 200) {
    $stringCut = substr($review1, 0, 200);
    $review1 = substr($stringCut, 0, strrpos($stringCut, ' ')).'...'; 
}


//Message pulled from array
        echo '<div class="review-txt" >';
        echo $review1;
        echo '</div>';
        
         $popup = "popup";
         $popid = strval($i);
        //Read more button
        echo '<br><div class="box">';
        echo '<a class="button" href="#';
        echo $popup + $popid;
        echo '">Read More</a>'; 

        echo '</div></div>';
        echo '<div id="';
        echo $popup + $popid;
        echo '" class="overlay">';
    ?>

    <div class="popup">
        <h2>Review</h2>
        <a class="close" href="#">&times;</a>
        <div class="content">
             <?php 


        echo '<div style=" "> <div style = " text-align: center; padding: 0; max-width: 150px; margin: auto; "> <img src="';
        echo $thearr[$i]['url'];
        echo '"/></div>';
        
        //Name of reviewer
        echo '<div style="text-align: center;"><h2>';
        echo $thearr[$i]['name'];
        echo '</h2></div>';
            
        //Start review generates here   
        echo '<div style = " text-align: center; padding-bottom: 20px; margin: auto; ">  ';
        if($thearr[$i]['rating'] === "&#9733 &#9733 &#9733 &#9733 &#9733" || $thearr[$i]['rating'] === "★ ★ ★ ★ ★"){ echo '<div class="thestar">  ★ ★ ★ ★ ★ </div>';}
        if($thearr[$i]['rating'] === "&#9733 &#9733 &#9733 &#9733 &#9734" || $thearr[$i]['rating'] === "★ ★ ★ ★ ☆"){ echo '<span class="thestar">  ★ ★ ★ ★</span> <span class="thatstar">★ </span>';}
        if($thearr[$i]['rating'] === "&#9733 &#9733 &#9733 &#9734 &#9734" || $thearr[$i]['rating'] === "★ ★ ★ ☆ ☆"){ echo '<span class="thestar">  ★ ★ ★ </span> <span class="thatstar">★ ★ </span>';}
        if($thearr[$i]['rating'] === "&#9733 &#9733 &#9734 &#9734 &#9734" || $thearr[$i]['rating'] === "★ ★ ☆ ☆ ☆"){ echo '<span class="thestar">  ★ ★ </span> <span class="thatstar">★ ★ ★ </span>';}
        if($thearr[$i]['rating'] === "&#9733 &#9734 &#9734 &#9734 &#9734" || $thearr[$i]['rating'] === "★ ☆ ☆ ☆ ☆"){ echo '<span class="thestar">  ★  </span> <div class="thatstar">★ ★ ★ ★ </span>';}
        echo '</div>';
        
        //Message pulled from array
        echo '<div class="review-txt">';
        //echo $thearr[$i]['review'];
    echo str_replace("\'","'",$thearr[$i]['review']);

            echo '</div>';

             ?>
            </div>
        </div>
    </div>
    </div>
 

        <?php

      }

        //end Wrap
        echo '</div>';

        //global WP theme-driven "after widget" code
        echo $after_widget;
    } 
    
    }