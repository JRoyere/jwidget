<?php

/*

    Plugin Name: JWidget
    Plugin URI: 
    Description: Displays 3 random widgets with a lightbox 
    Author: Jonathan Royere
    Author URI: http://panda-ant.com
    Version: 1.0.2

*/

// Commmit comment for branch fix_list
    //2
    //3
    //4
    //5
    //6
    //7
    //8
    //9
    
    
require_once 'JWidget.class.php';

require_once 'Jwidgetall.class.php';

function super_plugin_options(){  

    include('JWidget.class.php');
    include('JWidgetall.class.php');  
 }

function register_JWidget_widget(){

    register_widget('JWidget');

}


add_action('widgets_init','register_JWidget_widget');







function register_JWidgetall_widget(){

    register_widget('JWidgetall');

}


add_action('widgets_init','register_JWidgetall_widget');


global $custom_table_example_db_version;
$custom_table_example_db_version = '1.1'; 

/**
 * register_activation_hook implementation
 *
 * will be called when user activates plugin first time
 * must create needed database tables
 */
function custom_table_example_install()
{
    global $wpdb;
    global $custom_table_example_db_version;

    $table_name = $wpdb->prefix . 'cte'; // do not forget about tables prefix

   
    $sql = "CREATE TABLE " . $table_name . " (
      id int(11) NOT NULL AUTO_INCREMENT,
      name tinytext NOT NULL,
      url tinytext NOT NULL,
      rating tinytext NOT NULL,
      review TEXT NOT NULL,
      PRIMARY KEY  (id)
    );";

  
    require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
    dbDelta($sql);


    add_option('custom_table_example_db_version', $custom_table_example_db_version);

    $installed_ver = get_option('custom_table_example_db_version');
    if ($installed_ver != $custom_table_example_db_version) {
        $sql = "CREATE TABLE " . $table_name . " (
          id int(11) NOT NULL AUTO_INCREMENT,
          name tinytext NOT NULL,
          url tinytext NOT NULL,
          rating tinytext NOT NULL,
          review TEXT NOT NULL,
          PRIMARY KEY  (id)
        );";

        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        dbDelta($sql);

     
        update_option('custom_table_example_db_version', $custom_table_example_db_version);
    }
}

register_activation_hook(__FILE__, 'custom_table_example_install');


function custom_table_example_install_data()
{
    global $wpdb;

    $table_name = $wpdb->prefix . 'cte'; // do not forget about tables prefix

    $wpdb->insert($table_name, array(
        'name' => 'Alex Smith',
        'url' => 'https://cdn2.iconfinder.com/data/icons/rcons-user/32/male-fill-circle-512.png',
        'rating' => '&#9733 &#9733 &#9733 &#9733 &#9733',
        'review' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse sodales sapien sapien, et sodales augue interdum a. Suspendisse porta libero porttitor, maximus lorem nec, hendrerit elit. Proin ut maximus felis.'
    ));
    $wpdb->insert($table_name, array(
        'name' => 'Jordan Belford',
        'url' => 'https://cdn2.iconfinder.com/data/icons/rcons-user/32/male-fill-circle-512.png',
        'rating' => '&#9733 &#9733 &#9733 &#9733 &#9734',
        'review' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse sodales sapien sapien, et sodales augue interdum a. Suspendisse porta libero porttitor, maximus lorem nec, hendrerit elit. Proin ut maximus felis.'
    ));
    $wpdb->insert($table_name, array(
        'name' => 'Maria Laguerta',
        'url' => 'https://cdn2.iconfinder.com/data/icons/rcons-user/32/male-fill-circle-512.png',
        'rating' => '&#9733 &#9733 &#9733 &#9733 &#9733',
        'review' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse sodales sapien sapien, et sodales augue interdum a. Suspendisse porta libero porttitor, maximus lorem nec, hendrerit elit. Proin ut maximus felis.'
    ));
}

register_activation_hook(__FILE__, 'custom_table_example_install_data');

/**
 * Trick to update plugin database, see docs
 */
function custom_table_example_update_db_check()
{
    global $custom_table_example_db_version;
    if (get_site_option('custom_table_example_db_version') != $custom_table_example_db_version) {
        custom_table_example_install();
    }
}

add_action('plugins_loaded', 'custom_table_example_update_db_check');



if (!class_exists('WP_List_Table')) {
    require_once(ABSPATH . 'wp-admin/includes/class-wp-list-table.php');
}


class Custom_Table_Example_List_Table extends WP_List_Table
{
    
    function __construct()
    {
        global $status, $page;

        parent::__construct(array(
            'singular' => 'testimonial',
            'plural' => 'testimonials',
        ));
    }

   
    function column_default($item, $column_name)
    {
        return $item[$column_name];
    }

   
    function column_age($item)
    {
        return '<em>' . $item['review'] . '</em>';
    }


    function column_name($item)
    {
       
        $actions = array(
            'edit' => sprintf('<a href="?page=persons_form&id=%s">%s</a>', $item['id'], __('Edit', 'custom_table_example')),
            'delete' => sprintf('<a href="?page=%s&action=delete&id=%s">%s</a>', $_REQUEST['page'], $item['id'], __('Delete', 'custom_table_example')),
        );

        return sprintf('%s %s',
            $item['name'],
            $this->row_actions($actions)
        );
    }

    
    function column_cb($item)
    {
        return sprintf(
            '<input type="checkbox" name="id[]" value="%s" />',
            $item['id']
        );
    }

    function get_columns()
    {
        $columns = array(
            'cb' => '<input type="checkbox" />', //Render a checkbox instead of text
            'name' => __('Name', 'custom_table_example'),
            'url' => __('url', 'custom_table_example'),
            'rating' => __('Rating', 'custom_table_example'),
            'review' => __('Review', 'custom_table_example'),
        );
        return $columns;
    }

   
    function get_sortable_columns()
    {
        $sortable_columns = array(
            'name' => array('name', true),
            'url' => array('url', false),
            'rating' => array('rating', false),
            'review' => array('review', false),
        );
        return $sortable_columns;
    }

 
    function get_bulk_actions()
    {
        $actions = array(
            'delete' => 'Delete'
        );
        return $actions;
    }

   
    function process_bulk_action()
    {
        global $wpdb;
        $table_name = $wpdb->prefix . 'cte'; // do not forget about tables prefix

        if ('delete' === $this->current_action()) {
            $ids = isset($_REQUEST['id']) ? $_REQUEST['id'] : array();
            if (is_array($ids)) $ids = implode(',', $ids);

            if (!empty($ids)) {
                $wpdb->query("DELETE FROM $table_name WHERE id IN($ids)");
            }
        }
    }

   
    function prepare_items()
    {
        global $wpdb;
        $table_name = $wpdb->prefix . 'cte'; 

        $per_page = 10; 

        $columns = $this->get_columns();
        $hidden = array();
        $sortable = $this->get_sortable_columns();

        $this->_column_headers = array($columns, $hidden, $sortable);

        $this->process_bulk_action();

        $total_items = $wpdb->get_var("SELECT COUNT(id) FROM $table_name");

        $paged = isset($_REQUEST['paged']) ? max(0, intval($_REQUEST['paged'] -1) * $per_page) : 0;
        $orderby = (isset($_REQUEST['orderby']) && in_array($_REQUEST['orderby'], array_keys($this->get_sortable_columns()))) ? $_REQUEST['orderby'] : 'name';
        $order = (isset($_REQUEST['order']) && in_array($_REQUEST['order'], array('asc', 'desc'))) ? $_REQUEST['order'] : 'asc';


        $this->items = $wpdb->get_results($wpdb->prepare("SELECT * FROM $table_name ORDER BY $orderby $order LIMIT %d OFFSET %d", $per_page, $paged), ARRAY_A);

        $this->set_pagination_args(array(
            'total_items' => $total_items, // total items defined above
            'per_page' => $per_page, // per page constant defined at top of method
            'total_pages' => ceil($total_items / $per_page) // calculate pages count
        ));
    }
}

// ==== Admin section ====================
function custom_table_example_admin_menu()
{
    add_menu_page(__('Testimonials', 'custom_table_example'), __('Testimonials', 'custom_table_example'), 'activate_plugins', 'testimonials', 'custom_table_example_persons_page_handler');
    add_submenu_page('testimonials', __('Add new', 'custom_table_example'), __('Add new', 'custom_table_example'), 'activate_plugins', 'persons_form', 'custom_table_example_persons_form_page_handler');
}

add_action('admin_menu', 'custom_table_example_admin_menu');





function custom_table_example_persons_page_handler()
{
    global $wpdb;

    $table = new Custom_Table_Example_List_Table();
    $table->prepare_items();

    $message = '';
    if ('delete' === $table->current_action()) {
        $message = '<div class="updated below-h2" id="message"><p>' . sprintf(__('Items deleted: %d', 'custom_table_example'), count($_REQUEST['id'])) . '</p></div>';
    }
    ?>
<div class="wrap">


    <div class="icon32 icon32-posts-post" id="icon-edit"><br></div>
    <h2><?php _e('Testimonials', 'custom_table_example')?> <a class="add-new-h2"
                                 href="<?php echo  get_admin_url(get_current_blog_id(), 'admin.php?page=persons_form');?>"><?php _e('Add new', 'custom_table_example')?></a>
    </h2>
    <?php echo $message; ?>

    <form id="persons-table" method="GET">
        <input type="hidden" name="page" value="<?php echo $_REQUEST['page'] ?>"/>
        <?php $table->display() ?>
    </form>

</div>
<?php
}

function custom_table_example_persons_form_page_handler()
{
    global $wpdb;
    $table_name = $wpdb->prefix . 'cte'; // do not forget about tables prefix

    $message = '';
    $notice = '';

    // this is default $item which will be used for new records
    $default = array(
        'id' => 0,
        'name' => '',
        'url' => '',
        'rating' => '',
        'review' => '',
    );

    // here we are verifying does this request is post back and have correct nonce
    if (wp_verify_nonce($_REQUEST['nonce'], basename(__FILE__))) {
        // combine our default item with request params
        $item = shortcode_atts($default, $_REQUEST);
        // validate data, and if all ok save item to database
        // if id is zero insert otherwise update
        $item_valid = custom_table_example_validate_person($item);
        if ($item_valid === true) {
            if ($item['id'] == 0) {
                $result = $wpdb->insert($table_name, $item);
                $item['id'] = $wpdb->insert_id;
                if ($result) {
                    $message = __('Item was successfully saved', 'custom_table_example');
                } else {
                    $notice = __('There was an error while saving item', 'custom_table_example');
                }
            } else {
                $result = $wpdb->update($table_name, $item, array('id' => $item['id']));
                if ($result) {
                    $message = __('Item was successfully updated', 'custom_table_example');
                } else {
                    $notice = __('There was an error while updating item', 'custom_table_example');
                }
            }
        } else {
            // if $item_valid not true it contains error message(s)
            $notice = $item_valid;
        }
    }
    else {
        // if this is not post back we load item to edit or give new one to create
        $item = $default;
        if (isset($_REQUEST['id'])) {
            $item = $wpdb->get_row($wpdb->prepare("SELECT * FROM $table_name WHERE id = %d", $_REQUEST['id']), ARRAY_A);
            if (!$item) {
                $item = $default;
                $notice = __('Item not found', 'custom_table_example');
            }
        }
    }
    add_meta_box('persons_form_meta_box', 'Person data', 'custom_table_example_persons_form_meta_box_handler', 'person', 'normal', 'default');

    ?>
<div class="wrap">
    <div class="icon32 icon32-posts-post" id="icon-edit"><br></div>
    <h2><?php _e('Person', 'custom_table_example')?> <a class="add-new-h2"
                                href="<?php echo  + get_admin_url(get_current_blog_id(), 'admin.php?page=persons');?>"><?php _e('back to list', 'custom_table_example')?></a>
    </h2>

    <?php if (!empty($notice)): ?>
    <div id="notice" class="error"><p><?php echo $notice ?></p></div>
    <?php endif;?>
    <?php if (!empty($message)): ?>
    <div id="message" class="updated"><p><?php echo $message ?></p></div>
    <?php endif;?>

    <form id="form" method="POST">
        <input type="hidden" name="nonce" value="<?php echo wp_create_nonce(basename(__FILE__))?>"/>
        <?php /* NOTICE: here we storing id to determine will be item added or updated */ ?>
        <input type="hidden" name="id" value="<?php echo $item['id'] ?>"/>

        <div class="metabox-holder" id="poststuff">
            <div id="post-body">
                <div id="post-body-content">
                    <?php /* And here we call our custom meta box */ ?>
                    <?php do_meta_boxes('person', 'normal', $item); ?>
                    <input type="submit" value="<?php _e('Save', 'custom_table_example')?>" id="submit" class="button-primary" name="submit">
                </div>
            </div>
        </div>
    </form>
</div>
<?php
// Blaze It!
}

/**
 * This function use jquery to call media uploader
 *
 * @param $item
 */
function Jwidget_media_upload ($item){
// jQuery
wp_enqueue_script('jquery');
// This will enqueue the Media Uploader script
wp_enqueue_media();
?>
    <div>
        <th>
             <label for="image_url"><strong>Image</strong></label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        </th>
        <td>
          <!-- img  id="url2" src="< ?php echo esc_attr($item['url']) ?>" -->
          <input id="url" name="url" type="text" style="width: 75%" value=" <?php echo esc_attr($item['url']) ?>"
          size="50" class="code" placeholder="<?php  _e('Image URL', 'custom_table_example') ?>" required></input>
          

    <input type="button" name="upload-btn" id="upload-btn" class="button-secondary" value="Upload Image">
    <td>

</div>
<script type="text/javascript">
jQuery(document).ready(function($){
    $('#upload-btn').click(function(e) {
        e.preventDefault();
        var image = wp.media({ 
            title: 'Upload Image',
            // if true uploads multiple files
            multiple: false
        }).open()
        .on('select', function(e){
            // This will return the selected image from the Media Uploader, the result is an object
            var uploaded_image = image.state().get('selection').first();
            // We convert uploaded_image to a JSON object to make accessing it easier
            // Output to the console uploaded_image
            console.log(uploaded_image);
            var image_url = uploaded_image.toJSON().url;
            // Let's assign the url value to the input field
            $('#url').val(image_url);
            //$('#url2').val(image_url);
        });
    });
});
</script>
<?php
}


function custom_table_example_persons_form_meta_box_handler($item)
{
    ?>

<table cellspacing="2" cellpadding="5" style="width: 100%;" class="form-table">
    <tbody>
    <tr class="form-field">
        <th valign="top" scope="row">
            <label for="name"><?php _e('Name', 'custom_table_example')?></label>
        </th>
        <td>
            <input id="name" name="name" type="text" style="width: 95%" value="<?php echo esc_attr($item['name'])?>"
                   size="50" class="code" placeholder="<?php _e('Your name', 'custom_table_example')?>" required>
        </td>
    </tr>


    <?php echo Jwidget_media_upload($item); ?>

    <tr class="form-field">
        <th valign="top" scope="row">
            <label for="rating"><?php _e('Rating', 'custom_table_example')?></label>
        </th>
        <td>
            <select  id="rating" name="rating" >

                <option <?php  if(esc_attr($item['rating']) === "★ ☆ ☆ ☆ ☆"){ echo 'selected';} ?> >  &#9733 &#9734 &#9734 &#9734 &#9734 </option>
                <option <?php  if(esc_attr($item['rating']) === "★ ★ ☆ ☆ ☆"){ echo 'selected';} ?>>  &#9733 &#9733 &#9734 &#9734 &#9734 </option>
                <option <?php  if(esc_attr($item['rating']) === "★ ★ ★ ☆ ☆"){ echo 'selected';} ?>>  &#9733 &#9733 &#9733 &#9734 &#9734 </option>
                <option <?php  if(esc_attr($item['rating']) === "★ ★ ★ ★ ☆"){ echo 'selected';} ?>>  &#9733 &#9733 &#9733 &#9733 &#9734 </option>
                <option <?php  if(esc_attr($item['rating']) === "★ ★ ★ ★ ★"){ echo 'selected';} ?> >  &#9733 &#9733 &#9733 &#9733 &#9733 </option>
              </select>
        </td>
    </tr>



    <tr class="form-field">
        <th valign="top" scope="row">
            <label for="review"><?php _e('Review', 'custom_table_example')?></label>
        </th>
        <td>
            <textarea id="review" name="review" type="text" style="width: 95%" value="<?php echo esc_attr($item['review'])?>"
                   size="100" class="code" placeholder="<?php _e('Review', 'custom_table_example')?>" required><?php echo str_replace("\'","'",$item['review']);?></textarea>
        </td>
    </tr>
    </tbody>
</table>
<?php
}

function custom_table_example_validate_person($item)
{
    $messages = array();

    if (empty($item['name'])) $messages[] = __('Name is required', 'custom_table_example');
     //if (empty($item['name'])) $messages[] = __('Name is required', 'custom_table_example');
    //if (!empty($item['url'])) $messages[] = __('image URL is required', 'custom_table_example');
    // if (!empty($item['email']) && !is_email($item['email'])) $messages[] = __('E-Mail is in wrong format', 'custom_table_example');
    // if (!ctype_digit($item['age'])) $messages[] = __('Age in wrong format', 'custom_table_example');
    //if(!empty($item['age']) && !absint(intval($item['age'])))  $messages[] = __('Age can not be less than zero');
    //if(!empty($item['age']) && !preg_match('/[0-9]+/', $item['age'])) $messages[] = __('Age must be number');
    //...

    if (empty($messages)) return true;
    return implode('<br />', $messages);
}


function custom_table_example_languages()
{
    load_plugin_textdomain('custom_table_example', false, dirname(plugin_basename(__FILE__)));
}

add_action('init', 'custom_table_example_languages');





